-- Populate DB tables
-- 5f62894c-f269-42aa-961d-4910e7c9fbc4
-- 9ef013de-24a2-42b4-9091-1e4035b4d206
-- 2b06da98-1eb7-4336-9ae8-2d384b0d4fc9
INSERT INTO customer (id, username, firstname, lastname, email)
VALUES ('5f62894c-f269-42aa-961d-4910e7c9fbc4', 'Kypros', 'Chrys', 'kyprosc', 'kypros@domain.com');
INSERT INTO customer (id, username, firstname, lastname, email)
VALUES ('9ef013de-24a2-42b4-9091-1e4035b4d206', 'Elena', 'Toum', 'toumele', 'toumele@domain.com');

INSERT INTO product (id, name, value)
VALUES (1, 'Corn flakes', 3.45);
INSERT INTO product (id, name, value)
VALUES (2, 'Chocolate bar', 0.65);
INSERT INTO product (id, name, value)
VALUES (3, 'Whiskey', 24.99);

INSERT INTO inventory (id, stockQuantity, product_id)
VALUES (41, 3, 1);
INSERT INTO inventory (id, stockQuantity, product_id)
VALUES (5, 6, 2);
INSERT INTO inventory (id, stockQuantity, product_id)
VALUES (6, 9, 3);

INSERT INTO PurchaseOrder (id, status, customer_id)
VALUES (7, 'RECEIVED', '5f62894c-f269-42aa-961d-4910e7c9fbc4');
