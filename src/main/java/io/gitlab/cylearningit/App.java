package io.gitlab.cylearningit;

import org.h2.tools.RunScript;

import java.io.FileReader;

import io.gitlab.cylearningit.persistence.dao.CustomerDAO;
import io.gitlab.cylearningit.persistence.dao.impl.CustomerDAOImpl;
import io.gitlab.cylearningit.persistence.util.ConnectionProvider;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args ) throws Exception {
        System.out.println( "App started" );
        final App app = new App();
        app.prepareDB();

        app.checkCustomers();

        System.out.println( "App finished" );
    }

    private void prepareDB() throws Exception {
        createDBSchema();
        populateDB();
    }

    private void createDBSchema() throws Exception {
        final String bootstrapFilepath = ClassLoader.getSystemResource("persistence/create-schema.sql").getPath();
        RunScript.execute(ConnectionProvider.getConnection(), new FileReader(bootstrapFilepath));
    }

    private void populateDB() throws Exception {
        final String bootstrapFilepath = ClassLoader.getSystemResource("persistence/bootstrap.sql").getPath();
        RunScript.execute(ConnectionProvider.getConnection(), new FileReader(bootstrapFilepath));
    }

    private void checkCustomers() {
        CustomerDAO customerDAO = new CustomerDAOImpl();

        System.out.println(customerDAO.findAll());
    }
}
