package io.gitlab.cylearningit.persistence.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class ConnectionProvider implements AutoCloseable {
    private static final String DB_URL = "jdbc:h2:mem:testdb";
    private static final String USERNAME = "sa";
    private static final String PASSWORD = "Passw0rd";

    private static final ConnectionProvider  INSTANCE = new ConnectionProvider();

    public static Connection getConnection() {
        return INSTANCE.connection;
    }

    private final Connection connection;

    private ConnectionProvider() {
        try {
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void close() throws Exception {
        if (null != connection) {
            connection.close();
        }
    }
}
