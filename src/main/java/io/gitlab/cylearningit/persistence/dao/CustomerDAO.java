package io.gitlab.cylearningit.persistence.dao;

import java.util.UUID;

import io.gitlab.cylearningit.persistence.entities.Customer;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface CustomerDAO extends CrudDAO<Customer, UUID> {

}
