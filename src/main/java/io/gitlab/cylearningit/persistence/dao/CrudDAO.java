package io.gitlab.cylearningit.persistence.dao;

import java.util.Optional;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public interface CrudDAO<T, ID> {

    Iterable<T> findAll();

    Optional<T> findById(ID id);

    void deleteById(ID id);

    T save(T item);
}
