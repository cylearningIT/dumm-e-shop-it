package io.gitlab.cylearningit.persistence.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import io.gitlab.cylearningit.persistence.dao.CustomerDAO;
import io.gitlab.cylearningit.persistence.entities.Customer;
import io.gitlab.cylearningit.persistence.util.ConnectionProvider;

/**
 * @author <a href="mailto:kypros.chrysanthou@britebill.com">Kypros Chrysanthou</a>
 */
public class CustomerDAOImpl implements CustomerDAO {
    @Override
    public Iterable<Customer> findAll() {
        List<Customer> customerList = new ArrayList<>();
        try (PreparedStatement statement = ConnectionProvider.getConnection().prepareStatement("select * from customer")) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                final Customer customer = getCustomer(resultSet);
                customerList.add(customer);
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return customerList;
    }

    @Override
    public Optional<Customer> findById(UUID uuid) {
        Optional<Customer> customerOptional = Optional.empty();
        try (PreparedStatement statement = ConnectionProvider.getConnection().prepareStatement("select * from customer where id=?")) {
            statement.setObject(1, uuid);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final Customer customer = getCustomer(resultSet);

                customerOptional = Optional.of(customer);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return customerOptional;
    }

    @Override
    public void deleteById(UUID uuid) {
        try (PreparedStatement statement = ConnectionProvider.getConnection().prepareStatement("delete from customer where id=?")) {
            statement.setObject(1, uuid);

            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Customer save(Customer customer) {
        Customer savedCustomer = new Customer(customer);
        if (null == customer.getId()) {
            savedCustomer.setId(UUID.randomUUID());
        }

        final String insertQuery = "insert into customer (id, username, firstname, lastname, email) values (?, ?, ?, ?, ?)";
        try (PreparedStatement statement = ConnectionProvider.getConnection().prepareStatement(insertQuery)) {
            statement.setObject(1, savedCustomer.getId());
            statement.setString(2, savedCustomer.getUserName());
            statement.setString(3, savedCustomer.getFirstName());
            statement.setString(4, savedCustomer.getLastName());
            statement.setString(5, savedCustomer.getEmail());

            statement.execute();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return savedCustomer;
    }

    private Customer getCustomer(ResultSet resultSet) throws SQLException {
        final Customer customer = new Customer();
        String uuidStr = resultSet.getString("id");
        customer.setId(UUID.fromString(uuidStr));
        customer.setUserName(resultSet.getString("username"));
        customer.setFirstName(resultSet.getString("firstname"));
        customer.setLastName(resultSet.getString("lastname"));
        customer.setEmail(resultSet.getString("email"));
        return customer;
    }
}
